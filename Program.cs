﻿using System;

namespace LV6
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Ovo je naslov 1. biljeske", "Prva biljeska"));
            notebook.AddNote(new Note("Ovo je naslov 2. biljeske", "Druga biljeska"));
            notebook.AddNote(new Note("Ovo je naslov 3. biljeske", "Treca biljeska"));
            IAbstractIterator iterator = notebook.GetIterator();
            for (Note note = iterator.First(); !iterator.IsDone; note = iterator.Next())
            {
                note.Show();
            }
            Box box = new Box();
            box.AddProduct(new Product("1. proizvod", 1));
            box.AddProduct(new Product("2. proizvod", 50));
            box.AddProduct(new Product("3. proizvod", 23.25));
            IAbstractIterator2 iterator2 = box.GetIterator();
            for (Product product = iterator2.First(); !iterator2.IsDone; product = iterator2.Next())
            {
                Console.WriteLine(product.ToString());
            }
        }
    }
}
